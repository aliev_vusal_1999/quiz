﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace quizWpf.Models
{
    public class Theme 
    {
        public int Id { get; set; }
        public string ThemeName { get; set; }       
    }
}
