﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace quizWpf.Models
{
    public class Question
    {
        public int Id { get; set; }
        public string QuestionName { get; set; }
        public int Theme_Id { get; set; }
        public string Answer { get; set; }
    }
}
