﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Input;
using System.Windows;
//using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using quizWpf.Models;
using EasyHttp.Http;
using System.Windows.Controls;

namespace quizWpf
{

    public class RelayCommand : ICommand
    {
        private readonly Action _execute;
        private readonly Func<bool> _canExecute;

        public RelayCommand(Action execute, Func<bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        public void Execute(object parameter)
        {
            _execute.Invoke();
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute?.Invoke() ?? true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }

    public class RelayCommand<T> : ICommand
    {
        private readonly Action<T> _execute;
        private readonly Func<T, bool> _canExecute;

        public RelayCommand(Action<T> execute, Func<T, bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        public void Execute(object parameter)
        {
            if (parameter is T arg)
            {
                _execute.Invoke(arg);
            }
        }

        public bool CanExecute(object parameter)
        {
            if (parameter is T arg)
            {
                return _canExecute?.Invoke(arg) ?? true;
            }
            return false;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }

    public class MainViewModel : INotifyPropertyChanged
    {
        public MainViewModel()
        {
            Users = new ObservableCollection<User>();
            Themes = new ObservableCollection<Theme>();
            Questions = new ObservableCollection<Question>();

            LoadThemes();
            _auth = "Войти";
            _rating = 0;
            _answer = "";
            _correctly = "";
            _qanswer = "";
        }

        private string _themeName;
        private Theme _selectedTheme;
        private string _auth;
        private string _userName;
        private string _answer;
        private int _rating;
        private string _questionName;
        private string _correctly;
        private string _qanswer;
        private string _ansSend;
        public ObservableCollection<User> Users { get; set; }
        public ObservableCollection<Theme> Themes { get; set; }
        public ObservableCollection<Question> Questions { get; set; }

        // ICommand vars
        private ICommand _logIn;
        private ICommand _choice;
        private ICommand _send;

        public string ThemeName
        {
            get { return _themeName; }
            set { _themeName = value; OnPropertyChanged(nameof(ThemeName)); }
        }
        public Theme SelectedTheme
        {
            get { return _selectedTheme; }
            set { _selectedTheme = value; OnPropertyChanged(nameof(SelectedTheme)); }
        }      
        public string Auth
        {
            get { return _auth; }
            set { _auth = value; OnPropertyChanged(nameof(Auth)); }
        }
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; OnPropertyChanged(nameof(UserName)); }
        }
        public int Rating
        {
            get { return _rating; }
            set { _rating = value; OnPropertyChanged(nameof(Rating)); }
        }
        public string Answer
        {
            get { return _answer; }
            set { _answer = value; OnPropertyChanged(nameof(Answer)); }
        }
        public string QAnswer
        {
            get { return _qanswer; }
            set { _qanswer = value; }
        }
        public string QuestionName
        {
            get { return _questionName; }
            set { _questionName = value; OnPropertyChanged(nameof(QuestionName)); }
        }
        public string Correctly
        {
            get { return _correctly; }
            set { _correctly = value; OnPropertyChanged(nameof(Correctly)); }
        }
        public string AnsSend
        {
            get { return _ansSend; }
            set { _ansSend = value; OnPropertyChanged(nameof(AnsSend)); }
        }
        public void LoadThemes()
        {
            HttpClient client = new HttpClient();
            var response = client.Get("https://localhost:44395/api/Theme");
            Themes = response.StaticBody<ObservableCollection<Theme>>();
        }
        // Команда Войти        
        public ICommand LogIn
        {
            get { return _logIn ?? (_logIn = new RelayCommand<string>(LogInExecute, LogInCanExecute)); }
        }
        private void LogInExecute(string userName)
        {
            if (Auth == "Выйти")
            {
                UserName = "";
                Rating = 0;
                Correctly = "";
                QuestionName = "";
                AnsSend = "";
                Answer = "";
                Auth = "Войти";
                return;
            }
            HttpClient client = new HttpClient();
            var response = client.Get($"https://localhost:44395/api/User/{userName}");
            User apiUser = response.StaticBody<User>();
            if (apiUser != null)
            {
                UserName = userName;
                Rating = apiUser.Rating;
                Auth = "Выйти";
            }
            else
            {
                User user = new User();
                user.UserName = userName;
                user.Rating = 0;
                Users.Add(user);
                UserName = userName;
                Auth = "Выйти";
                client.Get($"https://localhost:44395/api/User/Add/{userName}");
            }
        }
        private bool LogInCanExecute(string userName)
        {
            if (userName != "")
                return true;
            else
                return false;
        }
        // Команда Выбрать
        public ICommand Choice
        {
            get { return _choice ?? (_choice = new RelayCommand<Theme>(ChoiceExecute, ChoiceCanExecute)); }
        }
        private void ChoiceExecute(Theme theme)
        {
            HttpClient client = new HttpClient();
            var response = client.Get($"https://localhost:44395/api/Question/{theme.Id}");
            Question question = response.StaticBody<Question>();
            QuestionName = question.QuestionName;
            QAnswer = question.Answer;
        }
        private bool ChoiceCanExecute(Theme theme)
        {
            if (theme != null && Auth != "Войти")
                return true;
            else
                return false;
        }
        // Команда Отправить
        public ICommand Send
        {
            get { return _send ?? (_send = new RelayCommand(SendExecute, SendCanExecute)); }
        }
        private void SendExecute()
        {
            HttpClient client = new HttpClient();
            if (AnsSend == QAnswer)
            {
                Correctly = "Верно!";
                Answer = QAnswer;
                Rating++;                
            }
            else
            {
                Correctly = "Неверно!";
                Answer = QAnswer;
                if (Rating > 0)
                    Rating--;
            }
            client.Get($"https://localhost:44395/api/User/Update/{UserName}/{Rating}");
            AnsSend = "";
        }
        private bool SendCanExecute()
        {
            if (AnsSend != null && AnsSend != "" && Auth != "Войти")
                return true;
            else
                return false;
        }







        public string Error => throw new NotImplementedException();
        public string this[string columnName] => throw new NotImplementedException();
        private void OnPropertyChanged(string PropertyName)
        {
            if (PropertyName == null)
                return;
            PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
